﻿using System.Windows.Input;
using Xamarin.Forms;

namespace lab12_calculator.ViewModels
{
    public class CalculatorViewModel : ViewModelBase
    {
        #region Properties
        double value1;
        public double Value1
        {
            get { return value1; }
            set
            {
                if (value1 != value)
                {
                    value1 = value;
                    OnPropertyChanged();
                }
            }
        }

        double value2;
        public double Value2
        {
            get { return value2; }
            set
            {
                if (value2 != value)
                {
                    value2 = value;
                    OnPropertyChanged();
                }
            }
        }

        string operation;
        public string Operation
        {
            get { return operation; }
            set
            {
                if (operation != value)
                {
                    operation = value;
                    OnPropertyChanged();
                }
            }
        }

        string result;
        public string Result
        {
            get { return result; }
            set
            {
                if (result != value)
                {
                    result = value;
                    OnPropertyChanged();
                }
            }
        }

        int currentState;
        public int CurrentState
        {
            get { return currentState; }
            set
            {
                if (currentState != value)
                {
                    currentState = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion

        #region Eventos 
        public ICommand OnSelectNumber { protected set; get; }
        public ICommand OnSelectOperator { protected set; get; }
        public ICommand OnClear { protected set; get; }
        public ICommand OnCalculate { protected set; get; }
        #endregion

        public CalculatorViewModel()
        {
            OnSelectNumber = new Command<string>(
                execute: (string arg) =>
                {
                    string number_pressed = arg;

                    if (Result == "0" || CurrentState < 0)
                    {
                        Result = "";
                        if (CurrentState < 0)
                        {
                            CurrentState *= -1;
                        }
                    }

                    Result += number_pressed;

                    if (double.TryParse(Result, out double number))
                    {
                        Result = number.ToString("N0");
                        if (CurrentState == 1)
                        {
                            Value1 = number;
                        }
                        else
                        {
                            Value2 = number;
                        }
                    }
                }
            );

            OnSelectOperator = new Command<string>(
                execute: (string arg) =>
                {
                    CurrentState = -2;
                    string operator_pressed = arg;

                    Operation = operator_pressed;
                }
            );

            OnClear = new Command(() =>
            {
                Value1 = 0;
                Value2 = 0;
                CurrentState = 1;
                Result = "0";
            });

            OnCalculate = new Command<string>(
                execute: (string arg) =>
                {
                    if (CurrentState == 2)
                    {
                        double result = 0;

                        switch (Operation)
                        {
                            case "+":
                                result = Value1 + Value2;
                                break;
                            case "-":
                                result = Value1 - Value2;
                                break;
                            case "x":
                                result = Value1 * Value2;
                                break;
                            case "÷":
                                result = Value1 / Value2;
                                break;
                        }

                        Result = result.ToString();
                        Value1 = result;
                        CurrentState = -1;
                    }
                }
            );
        }
    }
}
