﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab12_calculator.Models
{
    public class Calculator
    {
        public double Value1 { get; set; }
        public double Value2 { get; set; }
        public string Operation { get; set; }
        public string Result { get; set; }
        public int CurrentState { get; set; }
    }
}
