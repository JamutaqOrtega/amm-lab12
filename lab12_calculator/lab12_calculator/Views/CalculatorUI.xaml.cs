﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace lab12_calculator.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalculatorUI : ContentPage
    {
        public CalculatorUI()
        {
            InitializeComponent();
            this.BindingContext = new ViewModels.CalculatorViewModel();
        }
    }
}